import { IPriceService } from "./price.service.interface";

export class PriceService implements IPriceService{


	calculateTotalPrice(basePrice: number, state: string): number{
		const tax = Math.random();
		return basePrice + tax;
	}
}
