import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms"
import { Product } from '../product';
import { PriceService } from './price.service';
import {MockPriceService} from "./price.service.mock";


@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})
export class PriceComponent implements OnInit {
  form: FormGroup;

  productMock: Product;
  product: Product;
  serviceMock: MockPriceService;
  service: PriceService;

  totalMock: number;
  total: number;
  constructor(fb: FormBuilder) {
    this.form = fb.group({
      code: ['']
    });

    this.serviceMock = new MockPriceService();
    this.service = new PriceService();

    this.productMock = new Product(this.serviceMock, 10.00);
    this.product = new Product(this.service, 10.00);

  }

  mockSubmit(value: any){
    console.log("submit", value);
    this.totalMock = this.productMock.totalPrice(value.code);
  }

  serviceSubmit(value: any) {
    console.log("submit", value);
    this.total = this.product.totalPrice(value.code);
  }

  ngOnInit() {
  }

}
