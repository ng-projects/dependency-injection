import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  api: string;
  constructor(@Inject("API_URL") apiUrl: string) {
    this.api = apiUrl;
  }

  ngOnInit() {
  }

}
