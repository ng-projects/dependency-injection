import {Injectable} from "@angular/core";
import { Phrases } from "./phrases";
@Injectable()
export class LuckyPhraseService {
    phrase: any;
    phrases: Array<string> = Phrases;

    setPhrase () {
        this.phrase = this.phrases[Math.round (Math.random() * this.phrases.length)];
    }

    getPhrase(): any {
        return this.phrase;
    }
}