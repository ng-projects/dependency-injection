import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LuckyPhraseComponent } from './lucky-phrase.component';

describe('LuckyPhraseComponent', () => {
  let component: LuckyPhraseComponent;
  let fixture: ComponentFixture<LuckyPhraseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LuckyPhraseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LuckyPhraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
