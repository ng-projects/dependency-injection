import { Component, OnInit } from '@angular/core';
import { LuckyPhraseService } from './lucky-phrase.service';

@Component({
  selector: 'app-lucky-phrase',
  templateUrl: './lucky-phrase.component.html',
  styleUrls: ['./lucky-phrase.component.css']
})
export class LuckyPhraseComponent implements OnInit {

  phrase: string;

  constructor(private luckyPhraseService: LuckyPhraseService) {
    
  }

  generate(): void{
    this.luckyPhraseService.setPhrase();
    this.phrase = this.luckyPhraseService.getPhrase();
  }


  ngOnInit() {
  }

}
