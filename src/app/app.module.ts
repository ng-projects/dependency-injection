import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PriceComponent } from './price/price.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { UserComponent } from './user/user.component';
import { LuckyPhraseComponent } from './lucky-phrase/lucky-phrase.component';
import { LuckyPhraseService } from './lucky-phrase/lucky-phrase.service';
import { AnalyticsComponent } from './analytics/analytics.component';


@NgModule({
  declarations: [
    AppComponent,
    PriceComponent,
    UserComponent,
    LuckyPhraseComponent,
    AnalyticsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    LuckyPhraseService,
    {provide: "API_URL", useValue: "htt://test.api.com/v1"}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
